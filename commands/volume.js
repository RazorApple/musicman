module.exports = {
  name: "volume",
  description: "Adjust the volume.",
  execute: (client, message, args, queue) => {

    if(isNaN(args[0]))
      return message.channel.send("Argument isn't a number!");

    args[0] = +args[0]

    if(args[0] > 200 || args[0] < 1)
      return message.channel.send("Please select a number in the range of 1-200!");

    const serverQueue = queue.get(message.guild.id)

    if (!message.member.voice.channel)
      return message.channel.send("You aren't in a channel!");

    if (!serverQueue)
      return message.channel.send("No music playing!");


    serverQueue.connection.dispatcher.setVolume(args[0]/100);
    message.channel.send(client.info(`Volume changed to ${args[0]}.`, "Volume"))

  }
}
