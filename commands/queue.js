module.exports = {
  name: "queue",
  description: "Show the queue of this server",
  execute: (client, message, args, queue) => {
    let createdMessage = [];
    let ranking = 0;

    const serverQueue = queue.get(message.guild.id)

    if(!serverQueue)
      return message.channel.send("No music playing!");

    serverQueue.songs.forEach(song => {
      ranking += 1;
      createdMessage.push(`**${ranking}**. ${song.title}`)
    })

    message.channel.send(createdMessage.join("\n"), {split: true})
  }
}
