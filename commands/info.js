const Discord = require("discord.js");

module.exports = {
  name: "info",
  description: "Get info about the bot!",
  execute: (client, message, args) => {
      let seconds = (client.uptime / 1000);

      let time = {
        days: Math.floor(seconds / (3600*24)),
        hours: Math.floor(seconds % (3600*24) / 3600),
        minutes: Math.floor(seconds % 3600 / 60),
        seconds: Math.floor(seconds % 60),
        ms: Date.now() - message.createdTimestamp
      }

      const embed = client.info("", "Info")
          .addField(":page_facing_up: Uptime", `${time.days}d ${time.hours}h ${time.minutes}m ${time.seconds}s`, true)
          .addField(":ping_pong:  Latency", time.ms, true)
          .addField(":evergreen_tree: LibOpus verion", require("../package.json").dependencies["@discordjs/opus"].substring(1), true)
          .addField(":wine_glass: Creator", "https://dsc.bio/gom", true);

      message.channel.send(embed);
    }
};
