module.exports = {
  name: "stop",
  description: "Stop a song!",
  execute: (client, message, args, queue) => {
    const serverQueue = queue.get(message.guild.id)

    if (!message.member.voice.channel)
      return message.channel.send("You aren't in a channel!");

    serverQueue.songs = [];
    serverQueue.connection.dispatcher.end();
    message.channel.send(client.info("Stopped!", "Stop"))
  }
}
