module.exports = {
  name: "skip",
  description: "Skip a song!",
  execute: (client, message, args, queue) => {
    const serverQueue = queue.get(message.guild.id)

    if (!message.member.voice.channel)
      return message.channel.send("You aren't in a channel!");

    if (!serverQueue)
      return message.channel.send("No music playing!");

    serverQueue.connection.dispatcher.end();
    message.channel.send(client.info("Skipped!", "Skip"))
  }
}
