const clean = text => {
  if (typeof(text) === "string")
    return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
  else
      return text;
}

module.exports = {
  name: "eval",
  description: "(Owner Only) Eval Javascript",
  execute: (client, message, args, queue) => {
    if(message.author.id !== "384342022955466753") return;

    try {
      const code = args.join(" ");
      let evaled = eval(code);

      if (typeof evaled !== "string")
      evaled = require("util").inspect(evaled);

      message.channel.send(clean(evaled), {code: "xl"});
    } catch (err) {
      message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
    }
  }
}
