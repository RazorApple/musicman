var { google } = require('googleapis');
const Discord = require("discord.js");
const ytdl = require("ytdl-core");
const config = require("../config");

var youtube = google.youtube({
   version: 'v3',
   auth: config.tokens.yt
});

module.exports = {
  name: "play",
  description: "Play a song from Youtube!",
  execute: async (client, message, args, queue) => {
    const voiceChannel = message.member.voice.channel;
    const serverQueue = queue.get(message.guild.id)
    const songs = [];

    if(!args[0])
      return message.channel.send("Missing search.");

    let data = await youtube.search.list({part: 'snippet', q: args.join(" ")}).catch(error => console.log(error));

    if(!data.data.items.length)
      return message.channel.send("No search results found.");

    const rawSong = data.data.items[0];

    switch (rawSong.id.kind) {
      case "youtube#playlist":
        let playlistSongs = await youtube.playlistItems.list({part: 'snippet', playlistId: rawSong.id.playlistId, maxResults: 50});
        playlistSongs = playlistSongs.data.items

        playlistSongs.forEach(song => {
          songs.push({
              title: song.snippet.title,
              url: song.snippet.resourceId.videoId,
              thumbnail: song.snippet.thumbnails.high.url
          })
        })
      break;
      case "youtube#video":
        songs.push({
            title: rawSong.snippet.title,
            url: rawSong.id.videoId,
            thumbnail: rawSong.snippet.thumbnails.high.url
        })
      break;
      default:
        return message.channel.send("Query isn't a video!");
      }



    if (!voiceChannel)
      return message.channel.send("You're not in a channel!");

    const permissions = voiceChannel.permissionsFor(client.user);

    if (!permissions.has("CONNECT") || !permissions.has("SPEAK"))
      return message.channel.send("I need the permissions to join and speak in your voice channel!");


    if (!serverQueue) {
      const temporaryQueue = {
        textChannel: message.channel,
        voiceChannel: voiceChannel,
        connection: null,
        songs: [],
        volume: 5,
        playing: true
      };

      queue.set(message.guild.id, temporaryQueue);

      songs.forEach(song => temporaryQueue.songs.push(song));

      try {
        var connection = await voiceChannel.join();

        temporaryQueue.connection = connection;
        play(message.guild, temporaryQueue.songs[0], queue);
      } catch (err) {
        queue.delete(message.guild.id);
        return console.log(err);
      }
    } else {

      if(songs.map(a=>a.url).some(r => serverQueue.songs.map(a=>a.url).includes(r)))
        return message.channel.send("Duplicate songs are not allowed.");

      songs.forEach(song => serverQueue.songs.push(song));

      message.channel.send(client.info(`Added ${songs[songs.length-1].title} to the queue.`, "Queue | Play", songs[songs.length-1].thumbnail))
    }

    function play(guild, song, queue) {
      const serverQueue = queue.get(guild.id)

      if (!song) {
        serverQueue.voiceChannel.leave();
        queue.delete(guild.id);
        return;
      }

      message.channel.send(client.info(`Now Playing: ${song.title}!`, "Now Playing | Play", song.thumbnail))


      const dispatcher = serverQueue.connection
        .play(ytdl(song.url, {filter: "audioonly", highWaterMark: 1<<25, quality: "highestaudio"}))
        .on("finish", () => {
          serverQueue.songs.shift();
          play(guild, serverQueue.songs[0], queue);
        })
        .on("error", error => console.error(error));

      serverQueue.connection.voice.setSelfDeaf(true);
      dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
    }
  }
}
