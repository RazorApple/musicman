module.exports = {
  name: "invite",
  description: "Invite this bot",
  execute: (client, message, args) => {
      message.channel.send(client.info("Invite sent via DM!", "Invite"))
      message.author.send(client.info(`https://discord.com/api/oauth2/authorize?client_id=${client.user.id}&permissions=36700160&scope=bot`, "Invite"));
    }
}
