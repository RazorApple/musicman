const { prefix } = require('../config');

module.exports = {
	name: 'help',
	description: 'List all of my commands or info about a specific command',
  execute: (client, message, args) => {

		const { commands } = message.client;

		if (!args.length)
			return message.channel.send(client.info(`${commands.map(command => command.name).join(', ')}`, "Commands | Help"));

		const command = commands.get(args[0].toLowerCase());

		if (!command)
			return message.channel.send('Command not found.');

		message.channel.send(client.info(`Description: ${command.description}`, `${command.name} | Help`));
	},
};
