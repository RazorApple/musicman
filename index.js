const Discord = require("discord.js");
const fs = require('fs');
var dayjs = require('dayjs')();
const config = require("./config");
const client = new Discord.Client();
client.commands = new Discord.Collection();
const queue = new Discord.Collection();

client.info = (desc="", title="", thumb="") => new Discord.MessageEmbed()
.setTitle(title)
.setDescription(desc)
.setColor(config.color())
.setThumbnail(thumb)
.setFooter(`${client.user.username} | ${dayjs.format('MMM D, YYYY h:mm a Z')}`);

for (const file of fs.readdirSync('./commands')) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);
}

client.on("ready", () => {
  console.log(`Connected! Username: ${client.user.username}`)
  client.user.setActivity(`the piano! | ${config.prefix}help | v${config.version}`)
})

client.on("message", (message) => {
  const args = message.content.slice(config.prefix.length).trim().split(/ +/);
	const command = args.shift().toLowerCase();

  if (!client.commands.has(command)) return;

  try {
  	client.commands.get(command).execute(client, message, args, queue);
  } catch (o_o){}
})

client.login(config.tokens.discord)
