# Music Man

A really clean music bot.
This bot is literally all about music. It's called music man for a reason!  
I made this bot since I literally had no bot with features like Volume changing for free.  
This bot allows that! Invite it [here!](https://discord.com/api/oauth2/authorize?client_id=663394650400358401&permissions=36700160&scope=bot)

## Credits
[Most of the heavy lifting code](https://web.archive.org/web/20200908081930/https://www.gabrieltanner.org/blog/dicord-music-bot)   
[Command Handler](https://discordjs.guide)
